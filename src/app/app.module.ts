import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherService } from './services/weather.service';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { WeatherComponent } from './components/weather/weather.component';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [DatePipe, WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
