import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';


@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  keys = Object.keys;

  weatherData = [];
  currentDate: [];
  modifiedData: any;

  constructor(
    private _weatherService: WeatherService,
    private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.getWeatherData();
  }

  getWeatherData() {
    let currentDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this._weatherService.getWeatherData(currentDate).subscribe(
      (res) => {
        this.weatherData = res['weather'];
        console.log(this.weatherData);
      },
      (err) => { });
  }

}
